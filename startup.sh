#!/bin/bash

echo "Simulate startup ..."
sleep 10
source venv/bin/activate
gunicorn -w 2 -b 0.0.0.0:5000 application_name.app:app

